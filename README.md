# 图片合成插件 

给图片添加图片水印和文字

### 使用方法

##### 安装 
	composer require zhangzc/image-merge
##### 使用方法
	
```
use ImgMerge\ImgMerge;


/**
* $img->fontColor = '#ffffff'; //设置全局字体颜色  优先级低于 数组内color
* $img->fontType = 'simsun'; //设置全局字体 (字体名称 预留了两个默认【simsun，simli】字体【宋体，隶书】，字体查找逻辑，插件自带字体->Windows系统字体->项目运行根目录下的fonts目录)
* $img->fontSize = ''; //设置默认字号 
* $img->savePath = './savePath'; //设置保存到服务器目录 没有目录自动创建 
**/



$img = new ImgMerge();


  /**
     *
     * 图片合成
     * @param string $baseImgPath 原始图片
     * @param string $fileName  导出文件名称（空为导出数据流）如果设置了$img->savePath 文件名不能为空 图片将保存到服务器$img->savePath文件夹中 不设置$img->savePath 为下载图片
     * @param string|array $sealImg 待合成图片 图片地址默认合成再右下角 or 数组 ['path'=>'图片地址','location'=>'lt(l:左，r:右，t:上，b:下，c:中)|[x,y]']  TODO 暂不支持location为字符串
     * @param array $font 待添加的文字 （二维数组） eg:[['text'=>'test','location'=>'[x,y],'size'=>'20','color'=>'#000000','font'=>'simli']]
     * @return string 如果保存到服务器 返回路径+文件名
     */
	 
$img->index('./static/images/base.png','','./static/images/s.png');
```
or
```
$imgArr = [
    ['path'=>'s.png','location'=>[111,500]],
    ['path'=>'s.png','location'=>[22,11]],
];
$fontArr = [
    ['text'=>'test','location'=>[121,222],'size'=>'50','color'=>'#FF00FF','font'=>'simsun'],
    ['text'=>'testa','location'=>[0,222],'size'=>'50','color'=>'#0000FF','font'=>'simli']
];
$img->index('base.png','',$imgArr,$fontArr);

```
or
```

        $img->fontType = 'simli';
        $fontArr = [
            ['text'=>'测试','location'=>[121,1222],'size'=>'150','color'=>'#FF00FF'],
            ['text'=>'测试测试','location'=>[0,1822],'size'=>'150','color'=>'#0000FF','font'=>'simsun']
        ];
        $fileName = time();
        $fileSaveName = $img->index('./static/images/base.png',$fileName,'./static/images/s.png',$fontArr);
        var_dump($fileSaveName);

```
